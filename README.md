This repo contains demo of using **Hadoop counters** for tracking the map reduce job work. 

Wrapped with **Spring Hadoop** integration.

Hadoop counters are **VERY USEFUL** if you want to track events during your map reduce job, like occurrence of malformed data.

Of course it's possible to set property into Hadoop job configuration, but this isn't scalable, because every node of Hadoop cluster can have it's own configuration.

Run demo: Simply launch App class.

Demo will simply track how many temperatures were bigger then 25 or lower then 25. 

For input:
```
Brno,36,1996
Praha,35,1985
Brno,31,1985
Berlin,-18,1987
Paris,20,1987
Paris,21,1988
Brno,-18,1987
Praha,22,1943
Madrid,44,1944
Madrid,22,1933
Madrid,-17,1955
Berlin,-22,1966
Praha,-31,1976

```
App class will printout:

```
********* JOB STATISTICS ************
Bigger then average: 4
Lower then average: 9

```