package net.myhadoop.counter.countertest;

import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class WeatherAnalysisReducerAvg extends Reducer<Text, Text, Text, Text>{
	
	// constant value, again just for testing purposes.
	float avg = 25.0f;
	
    @Override
    protected void reduce(Text key, Iterable<Text> values,
                          Context context)
            throws IOException, InterruptedException {
    	for (Text text: values) {
    		String arr[] = text.toString().split(",");
    		long temperature = Long.parseLong(arr[1]);
    		if (temperature > avg) {
    			context.write(new Text(arr[0]), new Text(arr[1]));
    			context.getCounter(
    					WeatherAnalysisEnumStatistics.BIGGER_THEN_AVERAGE).
    					increment(1);
    		} else {
    			context.getCounter(WeatherAnalysisEnumStatistics.LOWER_THEN_AVERAGE).
    					increment(1);
    		}
    	}
    }
}