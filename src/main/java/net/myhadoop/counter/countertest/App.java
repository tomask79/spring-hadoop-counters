package net.myhadoop.counter.countertest;

import org.apache.hadoop.mapreduce.Job;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Demo of using Hadoop's counters.
 *
 */
public class App 
{
    public static void main( String[] args ) throws Exception
    {
    	/**
         * Start map-reduce job with Hadoop's counters.
         * 
         * (Again wrapped with Spring-Hadoop integration)
         */
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        final Job job = (Job) ctx.getBean("averageJob"); 
        System.out.println("********* JOB STATISTICS ************");
        System.out.println("Bigger then average: "
        		+job.getCounters().findCounter(
        				WeatherAnalysisEnumStatistics.BIGGER_THEN_AVERAGE).getValue());
        System.out.println("Lower then average: "
        		+job.getCounters().findCounter(
        				WeatherAnalysisEnumStatistics.LOWER_THEN_AVERAGE).getValue());
    }
}
