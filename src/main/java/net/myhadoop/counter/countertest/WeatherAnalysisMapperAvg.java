package net.myhadoop.counter.countertest;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

/**
 * Very simple mapper, just emmiting rows by constant key which results
 * in sending them into the same reducer...
 * 
 * Nothing fancy, but I'm testing here just Hadoops counters.
 * 
 * @author tomask79
 *
 */
public class WeatherAnalysisMapperAvg extends Mapper<LongWritable, Text, Text, Text> {
	@Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, 
    InterruptedException {
    	final WeatherItem item = new WeatherItem(value);
    	System.out.println(value.toString());
    	context.write(new Text("inputrow.from.hdfs"), 
    			new Text(item.getCity()+","+item.getTemperature()));
    }
}
