package net.myhadoop.counter.countertest;

/**
 * Enum for Hadoop's counters...One item here = name of hadoop job statistic. 
 * 
 * @author tomask79
 *
 */
public enum WeatherAnalysisEnumStatistics {
	BIGGER_THEN_AVERAGE,
	LOWER_THEN_AVERAGE
}
